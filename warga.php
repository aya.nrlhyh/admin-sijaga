<?php
$act=(isset($_GET['act']) ? strtolower($_GET['act']) : NULL);//$_GET[act];
if($act=='del'){
  $kd=$_GET['kd_keluarga'];
  $q=mysqli_query($connect, "DELETE FROM data_keluarga WHERE kd_keluarga='$kd'"); 
  $q=mysqli_query($connect, "DELETE FROM data_kendaraan WHERE kd_keluarga='$kd'"); 
  $q=mysqli_query($connect, "DELETE FROM data_anggota_keluarga WHERE kd_keluarga='$kd'"); 
    echo"<script>document.location.href='index.php?menu=warga'</script>";
}
elseif($act=='edit'){
  $kd_keluarga=$_GET['kd_keluarga']; 
  $cek = mysqli_query($connect, "SELECT * FROM data_keluarga WHERE kd_keluarga='$kd_keluarga'"); 
  while ($data=mysqli_fetch_array($cek)) { ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-reply" onclick="goBack()"></i> Edit Data Keluarga </h2>
              <script>
                 function goBack() {
                 window.history.back();
                 }
              </script>
              <div class="clearfix"></div>
            </div>
          <div class="x_content">
          <form id="demo-form2"  enctype="multipart/form-data" method="post" class="form-horizontal form-label-left">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor KK</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="no_kk" value="<?php echo $data['no_kk']; ?>" placeholder="Isikan Nomor KK sesuai Kartu Keluarga" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Alama</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea required="required" name="alamat" placeholder="Isikan Alamat Keluarga dengan Lengkap" class="form-control col-md-7 col-xs-12"><?php echo $data['alamat']; ?></textarea>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Usaha Sampingan</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="jenis_usaha" value="<?php echo $data['jenis_usaha']; ?>" placeholder="Isikan Usaha Sampingan (Rumah)" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <input type="reset" class="btn btn-danger" value="Batal">
                <input type="submit" name="edit" class="btn btn-primary" value="Edit Data Keluarga">
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>

<?php } 
  include "../../koneksi.php";
    if(isset($_POST['edit'])){
      $a=$_POST['no_kk'];
      $b=$_POST['alamat'];
      $c=$_POST['jenis_usaha'];
      $hasil = mysqli_query($connect, "UPDATE data_keluarga SET no_kk='$a', alamat='$b', jenis_usaha='$c'WHERE kd_keluarga='$kd_keluarga'");
      if ($hasil) {
          echo "<script>
          alert(\"Success!\");
          document.location=\"index.php?menu=warga\"
        </script>";
          }
          else {
          echo "<script>
          alert(\"Gagal!\");
          document.location=\"index.php?menu=warga\"
        </script>";
          }
      }
} else { ?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-book"></i> Data Keluarga yang Terdata di SIJAGA <small> 
      <a class="btn btn-sm btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Tambah Data Keluarga</a></small></h2>
      <div class="clearfix"></div>
    </div>
    <div class="table-responsive-sm">
      <table id="datatable" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>NO</th>
            <th>Nomor KK</th>
            <th>Alamat</th>
            <th>Jenis Usaha</th>
            <th>Persetujuan Jaring Kemitraan</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        <?php 
        $no=1;
        $query=mysqli_query($connect, "SELECT * FROM data_keluarga");
           while($d=mysqli_fetch_array($query)) { ?>
          <tr>
            <td><?php echo $no; ?></td>
            <td><a href="index.php?menu=detail_warga&kd_keluarga=<?php echo $d['kd_keluarga']; ?>"><?php echo $d['no_kk']; ?></a></td>
            <td><?php echo $d['alamat']; ?></td>
            <td><?php echo $d['jenis_usaha']; ?></td>
            <td><?php $kesediaan=$d['jaring_mitra']; 
            if ($kesediaan=="1") { echo "Bersedia"; } else { echo "Tidak Bersedia"; }?></td>
            <td align="center">
              <a  onclick="return confirm('Apakah anda yakin ingin menghapus data ini? Ini kemungkinan menghapus seluruh data yang berhubungan dengan data ini');" class="btn btn-danger" href="index.php?menu=warga&act=del&kd_keluarga=<?php echo $d['kd_keluarga']; ?>"><i class="fa fa-trash"></i></a>
              <a class="btn btn-warning" href="index.php?menu=warga&act=edit&kd_keluarga=<?php echo $d['kd_keluarga']; ?>"><i class="fa fa-pencil"></i></a>
              <a class="btn btn-primary" href="index.php?menu=detail_warga&kd_keluarga=<?php echo $d['kd_keluarga']; ?>"><i class="fa fa-eye"></i></a>
            </td> 
          </tr>
        <?php $no++; } ?>
        </tbody>
      </table>
      </div>
    <div>
  </div>
<?php } ?>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-body">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Tambah Data Keluarga</h2>
              <div class="clearfix"></div>
            </div>
          <div class="x_content">
          <form id="demo-form2"  enctype="multipart/form-data" method="post" class="form-horizontal form-label-left">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor KK<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="no_kk" required="required" placeholder="Isikan Nomor KK sesuai Kartu Keluarga" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea required="required" name="alamat" placeholder="Isikan Alamat Keluarga dengan Lengkap" class="form-control col-md-7 col-xs-12"></textarea>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Usaha Sampingan<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="jenis_usaha" required="required" placeholder="Isikan Usaha Sampingan (Rumah)" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Kesediaan Jaring Mitra<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <label><input type="checkbox" name="jaring_mitra" value="1"> Bersedia</label>&nbsp;
                <label><input type="checkbox" name="jaring_mitra" value="0"> Tidak Bersedia</label>
            </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <input type="reset" class="btn btn-danger" value="Batal">
                <input type="submit" name="simpan" class="btn btn-primary" value="Tambah Data Keluarga">
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
  include "../../koneksi.php";
    if(isset($_POST['simpan'])){
      $a=$_POST['no_kk'];
      $b=$_POST['alamat'];
      $c=$_POST['jenis_usaha'];
      $d=$_POST['jaring_mitra'];
      $hasil = mysqli_query($connect, "INSERT INTO data_keluarga (no_kk,alamat,jenis_usaha,jaring_mitra ) values ('$a','$b','$c','$d')");
      if ($hasil) {
            echo '<script language="javascript">alert("Success"); document.location="index.php?menu=warga";</script>';
          }
          else {
            echo '<script language="javascript">alert("Gagal"); document.location="index.php?menu=warga";</script>';
          }
      }
?>