<?php
$kd=$_GET['kd_keluarga']; 
$act=(isset($_GET['act']) ? strtolower($_GET['act']) : NULL);//$_GET[act];

if($act=='del'){
  $kd_anggota=$_GET['kd_anggota_keluarga'];
  $q=mysqli_query($connect, "DELETE FROM data_anggota_keluarga WHERE kd_anggota_keluarga='$kd_anggota'"); 
    echo"<script>document.location.href='index.php?menu=detail_warga&kd_keluarga=$kd'</script>";
}

elseif($act=='del_kendaraan'){
  $kd_kendaraan=$_GET['kd_kendaraan'];
  $q=mysqli_query($connect, "DELETE FROM data_kendaraan WHERE kd_kendaraan='$kd_kendaraan'"); 
          echo "<script>
          alert(\"Success!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
}

elseif($act=='edit_kendaraan'){
  $kd_kendaraan=$_GET['kd_kendaraan']; 
  $cek = mysqli_query($connect, "SELECT * FROM data_kendaraan WHERE kd_kendaraan='$kd_kendaraan'"); 
  while ($data=mysqli_fetch_array($cek)) { ?>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-reply" onclick="goBack()"></i> Edit Data Kendaraan </h2>
              <script>
                 function goBack() {
                 window.history.back();
                 }
              </script>
              <div class="clearfix"></div>
            </div>
          <div class="x_content">
          <form id="demo-form2" enctype="multipart/form-data" method="post" class="form-horizontal form-label-left">
            <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <select name="jenis" class="form-control col-md-12 col-xs-12">
                    <option value="<?php echo $data['jenis']; ?>">Roda <?php echo $data['jenis']; ?></option>
                    <option value="<?php echo $data['jenis']; ?>">--------------</option>
                    <option value="2">Roda 2</option>
                    <option value="3">Roda 3</option>
                    <option value="4">Roda 4</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <textarea required="required" name="keterangan" placeholder="Isikan Keterangan Kendaraan dengan Lengkap" class="form-control col-md-12 col-xs-12"><?php echo $data['keterangan']; ?></textarea>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 ">
                <input type="submit" name="edit_kendaraan" class="btn btn-primary" value="Edit Data Kendaraan">
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>

<?php } 

  include "../../koneksi.php";
    if(isset($_POST['edit_kendaraan'])){
      $a=$_POST['jenis'];
      $b=$_POST['keterangan'];
      $kd=$_GET['kd_keluarga'];
      $hasil = mysqli_query($connect, "UPDATE data_kendaraan SET jenis='$a', keterangan='$b' WHERE kd_kendaraan='$kd_kendaraan'");
      if ($hasil) {
          echo "<script>
          alert(\"Success!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
          }
          else {
          echo "<script>
          alert(\"Gagal!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
          }
      }
}
elseif($act=='edit_anggota'){
  $kd_anggota_keluarga=$_GET['kd_anggota_keluarga']; 
  $cek = mysqli_query($connect, "SELECT * FROM data_anggota_keluarga WHERE kd_anggota_keluarga='$kd_anggota_keluarga'"); 
  while ($data=mysqli_fetch_array($cek)) { ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-reply" onclick="goBack()"></i> Edit Data Anggota Keluarga</h2>
              <script>
                 function goBack() {
                 window.history.back();
                 }
              </script>
              <div class="clearfix"></div>
            </div>
          <div class="x_content">
          <form id="demo-form2"  enctype="multipart/form-data" method="post" class="form-horizontal form-label-left">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="nama_lengkap" value="<?php echo $data['nama_lengkap']; ?>" placeholder="Isikan Nama Lengkap" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="jk" required="required" class="form-control col-md-7 col-xs-12">
                    <option value="<?php echo $data['jk']; ?>"><?php echo $data['jk']; ?></option>
                    <option value="<?php echo $data['jk']; ?>">------------------</option>
                    <option value="Laki - Laki">Laki - Laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="tmp_lhr" value="<?php echo $data['tmp_lhr']; ?>" placeholder="Isikan Tempat Lahir" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="date" name="tgl_lhr" value="<?php echo $data['tgl_lhr']; ?>" placeholder="Isikan Tanggal Lahir" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Status dalam Keluarga<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="status" class="form-control col-md-7 col-xs-12">
                    <option value="<?php echo $data['status']; ?>"><?php echo $data['status']; ?></option>
                    <option value="<?php echo $data['status']; ?>">--------------------</option>
                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                    <option value="Istri">Istri</option>
                    <option value="Anak">Anak</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="pekerjaan" value="<?php echo $data['pekerjaan']; ?>" placeholder="Isikan Pekerjaan" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Keahlian / Keterampilan</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="keahlian" value="<?php echo $data['keahlian']; ?>" placeholder="Isikan Keahlian" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">No HP</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="no_hp" value="<?php echo $data['no_hp']; ?>" placeholder="Isikan No HP" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <input type="reset" class="btn btn-danger" value="Batal">
                <input type="submit" name="edit_anggota" class="btn btn-primary" value="Edit Data Anggota Keluarga">
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>

<?php } 

  include "../../koneksi.php";
    if(isset($_POST['edit_anggota'])){
      $a=$_POST['nama_lengkap'];
      $b=$_POST['tmp_lhr'];
      $c=$_POST['tgl_lhr'];
      $d=$_POST['jk'];
      $e=$_POST['no_hp'];
      $f=$_POST['pekerjaan'];
      $g=$_POST['keahlian'];
      $h=$_POST['status'];
      $hasil = mysqli_query($connect, "UPDATE data_anggota_keluarga SET nama_lengkap='$a', tmp_lhr='$b', tgl_lhr='$c', jk='$d', no_hp='$e', pekerjaan='$f', keahlian='$g', status='$h' WHERE kd_anggota_keluarga='$kd_anggota_keluarga'");
      if ($hasil) {
          echo "<script>
          alert(\"Success!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
          }
          else {
          echo "<script>
          alert(\"Gagal!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
          }
      }

}
else { 
$cek=mysqli_query($connect, "SELECT * FROM data_keluarga WHERE kd_keluarga='$kd'");
$data = mysqli_fetch_array($cek); ?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-reply" onclick="goBack()"></i> Detail Data Keluarga (Nomor KK : <?php echo $data['no_kk']; ?>)<small> 
        <script>
           function goBack() {
           window.history.back();
           }
        </script>
      <a class="btn btn-sm btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Tambah Data Anggota Keluarga</a>
      <a class="btn btn-sm btn-warning" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus"></i> Tambah Data Aset Kendaraan</a>
  </small></h2>
      <div class="clearfix"></div>
    </div>
    <div class="table-responsive-sm">
      <h2><b>Data Keluarga</b></h2>
      <div class="col-md-4">
      <table class="table">
          <tr>
            <td><b>No Kartu Kependudukan</b></td>
            <td><?php echo $data['no_kk']; ?></td>
          </tr>
          <tr>
            <td><b>Alamat</b></td>
            <td><?php echo $data['alamat']; ?></td>
          </tr>
          <tr>
            <td><b>Jenis Usaha</b></td>
            <td><?php echo $data['jenis_usaha']; ?></td>
          </tr>
          <tr>
            <td><b>Kesediaan Jaring Kemitraan</b></td>
            <td><?php $kesediaan=$data['jaring_mitra']; 
            if ($kesediaan=="1") { echo "Bersedia"; } else { echo "Tidak Bersedia"; }?></td>
          </tr>
      </table>
      </div>
      <div class="clearfix"></div>

      <h2><b>Daftar Anggota Keluarga</b></h2>
      <table id="datatable" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Lengkap</th>
            <th>Jns Kel.</th>
            <th>Tmp, Tgl Lahir</th>
            <th>Pekerjaan</th>
            <th>Status</th>
            <th>No HP</th>
            <th>Keterampilan / Keahlian</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        <?php 
        $no=1;
        $query=mysqli_query($connect, "SELECT * FROM data_anggota_keluarga WHERE kd_keluarga='$kd'");
           while($d=mysqli_fetch_array($query)) { ?>
          <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $d['nama_lengkap']; ?></td>
            <td><?php echo $d['jk']; ?></td>
            <td><?php echo $d['tmp_lhr'].', '.$d['tgl_lhr']; ?></td>
            <td><?php echo $d['pekerjaan']; ?></td>
            <td><?php echo $d['status']; ?></td>
            <td><?php echo $d['no_hp']; ?></td>
            <td><?php echo $d['keahlian']; ?></td>
            <td>
              <a class="btn btn-danger" href="index.php?menu=detail_warga&act=del&kd_keluarga=<?php echo $d['kd_keluarga']; ?>&kd_anggota_keluarga=<?php echo $d['kd_anggota_keluarga']; ?>"><i class="fa fa-trash"></i></a>
              <a class="btn btn-warning" href="index.php?menu=detail_warga&act=edit_anggota&kd_keluarga=<?php echo $d['kd_keluarga']; ?>&kd_anggota_keluarga=<?php echo $d['kd_anggota_keluarga']; ?>"><i class="fa fa-pencil"></i></a>
            </td> 
          </tr>
        <?php $no++; } ?>
        </tbody>
      </table>


      <h2><b>Daftar Aset Kepemilikan Kendaraan</b></h2>
      <table id="datatable" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Jenis Kendaraan</th>
            <th>Keterangan</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        <?php 
        $no=1;
        $query_kendaraan=mysqli_query($connect, "SELECT * FROM data_kendaraan WHERE kd_keluarga=$kd");
           while($data_kendaraan=mysqli_fetch_array($query_kendaraan)) { ?>
          <tr>
            <td><?php echo $no; ?></td>
            <td>Roda <?php echo $data_kendaraan['jenis']; ?></td>
            <td><?php echo $data_kendaraan['keterangan']; ?></td>
            <td>
              <a class="btn btn-danger" href="index.php?menu=detail_warga&act=del_kendaraan&kd_keluarga=<?php echo $kd; ?>&kd_kendaraan=<?php echo $data_kendaraan['kd_kendaraan']; ?>"><i class="fa fa-trash"></i></a>
              <a class="btn btn-warning" href="index.php?menu=detail_warga&act=edit_kendaraan&kd_keluarga=<?php echo $kd; ?>&kd_kendaraan=<?php echo $data_kendaraan['kd_kendaraan']; ?>"><i class="fa fa-pencil"></i></a>
            </td> 
          </tr>
        <?php $no++; } ?>
        </tbody>
      </table>
      </div>
    <div>
  </div>
<?php } ?>


<!---DATA ANGGOTA KELUARGA--->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-body">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Tambah Data Anggota Keluarga</h2>
              <div class="clearfix"></div>
            </div>
          <div class="x_content">
          <form id="demo-form2"  enctype="multipart/form-data" method="post" class="form-horizontal form-label-left">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="nama_lengkap" required="required" placeholder="Isikan Nama Lengkap" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="jk" required="required" class="form-control col-md-7 col-xs-12">
                    <option value="Laki - Laki">Laki - Laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="tmp_lhr" required="required" placeholder="Isikan Tempat Lahir" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="date" name="tgl_lhr" required="required" placeholder="Isikan Tanggal Lahir" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Status dalam Keluarga<span class="required"> *</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="status" required="required" class="form-control col-md-7 col-xs-12">
                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                    <option value="Istri">Istri</option>
                    <option value="Anak">Anak</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="pekerjaan" placeholder="Isikan Pekerjaan" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Keahlian / Keterampilan</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="keahlian" placeholder="Isikan Keahlian" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">No HP</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="no_hp" placeholder="Isikan No HP" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <input type="reset" class="btn btn-danger" value="Batal">
                <input type="submit" name="simpan" class="btn btn-primary" value="Tambah Data Anggota Keluarga">
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
  include "../../koneksi.php";
    if(isset($_POST['simpan'])){
      $a=$_POST['nama_lengkap'];
      $b=$_POST['tmp_lhr'];
      $c=$_POST['tgl_lhr'];
      $d=$_POST['jk'];
      $e=$_POST['no_hp'];
      $f=$_POST['pekerjaan'];
      $g=$_POST['keahlian'];
      $h=$_POST['status'];
      $hasil = mysqli_query($connect, "INSERT INTO data_anggota_keluarga (nama_lengkap,kd_keluarga,tmp_lhr,tgl_lhr,jk,no_hp,pekerjaan,keahlian,status) values ('$a','$kd','$b','$c','$d','$e','$f','$g','$h')");
      if ($hasil) {
          echo "<script>
          alert(\"Success!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
          }
          else {
          echo "<script>
          alert(\"Gagal!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
          }
      }
?>


<!---DATA ASET KENDARAAN--->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-body">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Tambah Data <br><small>Aset Kepemilikan Kendaraan</small></h2>
              <div class="clearfix"></div>
            </div>
          <div class="x_content">
          <form id="demo-form2" enctype="multipart/form-data" method="post" class="form-horizontal form-label-left">
            <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <select name="jenis" class="form-control col-md-12 col-xs-12">
                    <option value="2">Roda 2</option>
                    <option value="3">Roda 3</option>
                    <option value="4">Roda 4</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <textarea required="required" name="keterangan" placeholder="Isikan Keterangan Kendaraan dengan Lengkap" class="form-control col-md-12 col-xs-12"></textarea>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 ">
                <input type="submit" name="simpan_kendaraan" class="btn btn-primary" value="Tambah Data Aset">
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php
  include "../../koneksi.php";
    if(isset($_POST['simpan_kendaraan'])){
      $a=$_POST['jenis'];
      $b=$_POST['keterangan'];
      $kd=$_GET['kd_keluarga'];
      $hasil = mysqli_query($connect, "INSERT INTO data_kendaraan (jenis, keterangan, kd_keluarga) values ('$a','$b','$kd')");
      if ($hasil) {
          echo "<script>
          alert(\"Success!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
          }
          else {
          echo "<script>
          alert(\"Gagal!\");
          document.location=\"index.php?menu=detail_warga&kd_keluarga=$kd\"
        </script>";
          }
      }
?>
