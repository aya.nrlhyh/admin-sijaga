	    <a href="index.php?menu=warga">
      <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="tile-stats">
              <div class="icon"><i class="fa fa-users"></i></div>
                <div class="count">
                <?php
                $a=mysqli_query($connect, "SELECT * FROM data_anggota_keluarga");
                $b=mysqli_num_rows($a);
                echo $b;
                ?></div>
                <h3>Data Warga</h3>
                <p>Jumlah Data Warga</p>
          </div>
      </div></a>
      <a href="index.php?menu=kelola_user">
      <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="tile-stats">
              <div class="icon"><i class="fa fa-user"></i></div>
                <div class="count">
                <?php
                $a=mysqli_query($connect, "SELECT * FROM user");
                $b=mysqli_num_rows($a);
                echo $b;
                ?></div>
                <h3>Data User</h3>
                <p>Jumlah Data Pengguna Aplikasi</p>
          </div>
      </div></a>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel" style="min-height: 0">
    <div class="x_content"> 
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row top_tiles">
          <center><div class="product_price"><h1 align="center">Hallo, Administrator !!!</h1></div>
          <h5>Last Login : <?php echo date('h:m:s , d-m-Y',strtotime($last_login));?> </h5>
          <small><i class="fa fa-check"></i> You Logged As Administrator</small><br>
          <h3>Selamat Datang di Aplikasi </h3><b>SIJAGA (Sistem Informasi Jaringan Warga)</b><br> Silahkan kelola data data anda. <br>Selamat bekerja, jangan lupa logout setelah selesai menggunakan aplikasi ini<hr></center>
        </div>
      </div>
      <div id="map" style="height:400px; width: 100%;"></div><hr>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha256-yNbKY1y6h2rbVcQtf0b8lq4a+xpktyFc3pSYoGAY1qQ=" crossorigin="anonymous"></script>
      <!-- Custom script file -->
      <script src="../../assets/scripts/script.js"></script>
      <!-- Google Maps JS API -->
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDecXIc-lwF6B2o3DMUKUVifah38xiB8vs"></script>
    </div>
  </div>
</div>
  <script src="../../assets/js/highcharts.js"></script>
  <script src="../../assets/js/jquery-1.10.1.min.js"></script>
  <script>
    var chart; 
    $(document).ready(function() {
        chart = new Highcharts.Chart(
        {
         chart: {
          renderTo: 'mygraph',
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false
         },   
         title: {
          text: ''
         },
         tooltip: {
          formatter: function() {
            return '<b>'+
            this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 2) +' % ';
          }
         },
         plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              color: '#000000',
              connectorColor: 'green',
              formatter: function() 
              {
                return '<b>' + this.point.name + '</b>: ' + Highcharts.numberFormat(this.percentage, 2) +' % ';
              }
            }
          }
         },
          series: [{
          type: 'pie',
          name: 'Grafik Status',
          data: [
          <?php
            include "../../koneksi.php";
            $query = mysqli_query($connect,"SELECT status from grafik");
            while ($row = mysqli_fetch_array($query)) {
            $status = $row['status']; 
              $data = mysqli_fetch_array(mysqli_query($connect,"SELECT total from grafik where status='$status'"));
              $jumlah = $data['total'];
              ?>
              [ 
                '<?php echo $status ?>', <?php echo $jumlah; ?>
              ],
              <?php
            }
            ?>
          ]
        }]
        });
    }); 
  </script>
<script src="../../assets/js/highcharts.js"></script>
<script src="../../assets/js/jquery-1.10.1.min.js"></script>