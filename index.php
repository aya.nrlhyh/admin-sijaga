<?php
session_start();
if(!isset($_SESSION['id_user'])){
echo"<script>document.location.href='../index.php'</script>";
} 
else {
include "../../koneksi.php";
$q=mysqli_query($connect, "SELECT * FROM user where id_user='$_SESSION[id_user]' AND level='administrator' AND active='1'");
while($d=mysqli_fetch_array($q))
    {
      $id_user=$d['id_user'];
      $username=$d['username'];
      $last_login=$d['last_login'];
}
date_default_timezone_set('Asia/Kuala_Lumpur');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SIJAGA Dashboard | ADMINISTRATOR</title>
  <link rel="shortcut-icon" href="../assets/images/fav.jpg">
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="../assets/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="../assets/css/animate.min.css" rel="stylesheet">
  <link href="../assets/css/custom.css" rel="stylesheet">
  <link href="../assets/css/icheck/flat/green.css" rel="stylesheet">
  <link href="../assets/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script src="../assets/js/jquery.min.js"></script>
  <script src="../assets/js/highcharts.js"></script>
  <script type="text/javascript">
  function PreviewImage() {
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
  oFReader.onload = function (oFREvent)
   {
    document.getElementById("uploadPreview").src = oFREvent.target.result;
  };
  };
  </script>
  <script type="text/javascript">
  $(document).ready(function(){   
    $('.form-checkbox').click(function(){
      if($(this).is(':checked')){
        $('.form-password').attr('type','text');
      }else{
        $('.form-password').attr('type','password');
      }
    });
  });
  </script>
</head>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;"><a href="index.php?menu=home" class="site_title"><center><span>DASHBOARD</span></center></a>
          </div>
          <div class="clearfix"></div>
          <div class="profile">
            <div class="profile_pic"><img src="../assets/images/user.png" alt="..." class="img-circle profile_img"></div>
            <div class="profile_info"><span>Welcome,</span><h2><?php echo $username; ?></h2></div>
          </div>
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <ul class="nav side-menu" style="margin-top:100px">
                <li><a href="index.php?menu=home"><i class="fa fa-home"></i> Home</a></li>
                <br><h3>Manajemen Data</h3><br>
                <li><a href="index.php?menu=kelola_user"><i class="fa fa-user"></i> Manajemen User</a></li>
                <li><a href="index.php?menu=warga"><i class="fa fa-users"></i> Data Warga</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    <div class="top_nav">
      <div class="nav_menu">
        <nav class="" role="navigation">
          <div class="nav toggle"><a id="menu_toggle"><i class="fa fa-bars"></i></a></div>
            <ul class="nav navbar-nav navbar-right">
              <li class=""><a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <img src="../assets/berkas_upload/user.png" height="60px" alt="">Hallo, <?php echo $username; ?> !
                            <span class=" fa fa-angle-down"></span></a>
                  <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">                 
                    <li><a href="index.php?menu=ubah_pass&id_user=<?php echo $id_user; ?>">Change Password</a></li>
                    <li><a href="index.php?menu=logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
              </li>

                <li role="presentation" class="dropdown">
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <?php
                    while ($message=mysqli_fetch_array($hitung_message)) { ?>
                    <li>
                      <a href="index.php?menu=inbox&act=read&id_message=<?php echo $message['id_message']; ?>">
                          <span><b><?php echo $message['dari']; ?></b></span>
                          <span class="time"><?php echo $message['tgl']; ?></span>
                       
                        <span class="message">
                    <p><?php $inbox=$message['isi']; echo substr($inbox,0,30); ?>... </p>
                        </span>
                      </a>
                    </li>
                  <?php } ?>
                    <li>
                      <div class="text-center">
                        <a href="index.php?menu=inbox">
                          <strong>
                            <?php
                              $hitung_message=mysqli_query($connect, "SELECT * FROM message WHERE kepada='$id_user' AND status_read='0'");
                              $hitung=mysqli_num_rows($hitung_message);
                              if($hitung=='0') {
                                echo "Anda Tidak Memiliki Pesan"; }
                                else { 
                                  echo "See All Messages 
                          <i class='fa fa-angle-right'></i>"; }
                                  ?></strong>
                        </a>
                      </div>
                    </li>
            </ul>
        </nav>
      </div>
    </div>
    <div class="right_col" role="main">
      <div class="row">
      <?php
        if (isset($_GET['menu'])){
          $menu=$_GET['menu'];
          if($menu=="home"){include "home.php";}
          if($menu=="kelola_user"){include "kelola_user.php";}
          if($menu=="warga"){include "warga.php";}
          if($menu=="detail_warga"){include "detail_warga.php";}
          if($menu=="aset"){include "aset.php";} 
          if($menu=="ubah_pass"){include "ubah_pass.php";} 
          if($menu=="logout"){include "logout.php";} }
        else{include"home.php";}
        ?>
        </div>
        <footer>
          <div class="pull-right">SIJAGA - Dashboard 2019. All Rights Reserved. </div>
          <div class="clearfix"></div>
        </footer>
    </div>
  </div>
</div>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="../assets/js/icheck/icheck.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/js/datatables/dataTables.bootstrap.js"></script>
<script src="../assets/js/datatables/dataTables.buttons.min.js"></script>
<script src="../assets/js/datatables/buttons.bootstrap.min.js"></script>
<script src="../assets/js/datatables/jszip.min.js"></script>
<script src="../assets/js/datatables/pdfmake.min.js"></script>
<script src="../assets/js/datatables/vfs_fonts.js"></script>
<script src="../assets/js/datatables/buttons.html5.min.js"></script>
<script src="../assets/js/datatables/buttons.print.min.js"></script>
<script src="../assets/js/datatables/dataTables.fixedHeader.min.js"></script>
<script src="../assets/js/datatables/dataTables.keyTable.min.js"></script>
<script src="../assets/js/datatables/dataTables.responsive.min.js"></script>
<script src="../assets/js/datatables/responsive.bootstrap.min.js"></script>
<script src="../assets/js/datatables/dataTables.scroller.min.js"></script>
<script src="../assets/js/chartjs/chart.min.js"></script>
<script src="../assets/js/pace/pace.min.js"></script>
        <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>
      <script>
      Chart.defaults.global.legend = {
        enabled: false
      };

      // Line chart
      var ctx = document.getElementById("lineChart");
      var lineChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [{
            label: "My First dataset",
            backgroundColor: "rgba(38, 185, 154, 0.31)",
            borderColor: "rgba(38, 185, 154, 0.7)",
            pointBorderColor: "rgba(38, 185, 154, 0.7)",
            pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointBorderWidth: 1,
            data: [31, 74, 6, 39, 20, 85, 7]
          }, {
            label: "My Second dataset",
            backgroundColor: "rgba(3, 88, 106, 0.3)",
            borderColor: "rgba(3, 88, 106, 0.70)",
            pointBorderColor: "rgba(3, 88, 106, 0.70)",
            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(151,187,205,1)",
            pointBorderWidth: 1,
            data: [82, 23, 66, 9, 99, 4, 2]
          }]
        },
      });

      // Bar chart
      var ctx = document.getElementById("mybarChart");
      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [{
            label: '# of Votes',
            backgroundColor: "#26B99A",
            data: [51, 30, 40, 28, 92, 50, 45]
          }, {
            label: '# of Votes',
            backgroundColor: "#03586A",
            data: [41, 56, 25, 48, 72, 34, 12]
          }]
        },

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });

      // Doughnut chart
      var ctx = document.getElementById("canvasDoughnut");
      var data = {
        labels: [
          "Dark Grey",
          "Purple Color",
          "Gray Color",
          "Green Color",
          "Blue Color"
        ],
        datasets: [{
          data: [120, 50, 140, 180, 100],
          backgroundColor: [
            "#455C73",
            "#9B59B6",
            "#BDC3C7",
            "#26B99A",
            "#3498DB"
          ],
          hoverBackgroundColor: [
            "#34495E",
            "#B370CF",
            "#CFD4D8",
            "#36CAAB",
            "#49A9EA"
          ]

        }]
      };

      var canvasDoughnut = new Chart(ctx, {
        type: 'doughnut',
        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
        data: data
      });

      // Radar chart
      var ctx = document.getElementById("canvasRadar");
      var data = {
        labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
        datasets: [{
          label: "My First dataset",
          backgroundColor: "rgba(3, 88, 106, 0.2)",
          borderColor: "rgba(3, 88, 106, 0.80)",
          pointBorderColor: "rgba(3, 88, 106, 0.80)",
          pointBackgroundColor: "rgba(3, 88, 106, 0.80)",
          pointHoverBackgroundColor: "#fff",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          data: [65, 59, 90, 81, 56, 55, 40]
        }, {
          label: "My Second dataset",
          backgroundColor: "rgba(38, 185, 154, 0.2)",
          borderColor: "rgba(38, 185, 154, 0.85)",
          pointColor: "rgba(38, 185, 154, 0.85)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)",
          data: [28, 48, 40, 19, 96, 27, 100]
        }]
      };

      var canvasRadar = new Chart(ctx, {
        type: 'radar',
        data: data,
      });

      // Pie chart
      var ctx = document.getElementById("pieChart");
      var data = {
        datasets: [{
          data: [120, 50, 140, 180, 100],
          backgroundColor: [
            "#455C73",
            "#9B59B6",
            "#BDC3C7",
            "#26B99A",
            "#3498DB"
          ],
          label: 'My dataset' // for legend
        }],
        labels: [
          "Dark Gray",
          "Purple",
          "Gray",
          "Green",
          "Blue"
        ]
      };

      var pieChart = new Chart(ctx, {
        data: data,
        type: 'pie',
        otpions: {
          legend: false
        }
      });

      // PolarArea chart
      var ctx = document.getElementById("polarArea");
      var data = {
        datasets: [{
          data: [120, 50, 140, 180, 100],
          backgroundColor: [
            "#455C73",
            "#9B59B6",
            "#BDC3C7",
            "#26B99A",
            "#3498DB"
          ],
          label: 'My dataset' // for legend
        }],
        labels: [
          "Dark Gray",
          "Purple",
          "Gray",
          "Green",
          "Blue"
        ]
      };

      var polarArea = new Chart(ctx, {
        data: data,
        type: 'polarArea',
        options: {
          scale: {
            ticks: {
              beginAtZero: true
            }
          }
        }
      });
    </script>
</body>
</html>
<?php
}
?>